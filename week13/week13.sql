select count(*) from proteins;

explain select * from proteins where pin like "5HT2C_HUMA";

create index idx on proteins(pid);
create unique index idx on proteins(accession);
create unique index idx on proteins(accession,pid);
alter table proteins  add constraint acc_pk primary key (accession);

alter table 	proteins  drop index idx3; 
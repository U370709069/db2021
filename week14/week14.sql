use company;

create temporary table TopSuplliers (
		SupplierID int primary key,
        SupplierName varchar(45),
        ProductCount int
);

insert into TopSuplliers(SupplierID,SupplierName, ProductCount)
select suppliers.SupplierID,SupplierName, count(ProductID) as ProductCount
from suppliers join products on suppliers.SupplierID= products.SupplierID
group by SupplierID
order by ProductCount desc;

select * from TopSuplliers where ProductCount>3 
order by ProductCount desc;

select *from  customers;

call check_table_exists('TopSuppliers');
drop temporary table TopSuppliers;

update customers
set CustomerName="Ali Akgol",City="Hatay",PostalCode="31200",Country="Turkey"
where CustomerID=1;

delete from customers
where CustomerID=1;

truncate table Customers; ## I wrote it capital letter C , do not run it accidently
delete from Customers;## I wrote it capital letter C , do not run it accidently
drop table Customers;## I wrote it capital letter C , do not run it accidently